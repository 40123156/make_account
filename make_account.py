import hashlib
import string
import random
write_data = ""
 
''' sha1 secure hashes '''
 
# convert user_password into sha1 encoded string
def gen_password(user_password):
    return hashlib.sha1(user_password.encode("utf-8")).hexdigest()

# read lines from file
lines = open("stud_list", "r", encoding="utf-8").read().splitlines()
 
# we may also need to notice every user with computer generated passwords
for i in range(len(lines)):
    password = lines[i].split("\t")[2]
    print(lines[i].split("\t")[0], "-", password, "-", gen_password(password), \
            "-", lines[i].split("\t")[1])
    write_data = write_data + lines[i].split("\t")[0] + ":" + gen_password(password) + \
    ":" + lines[i].split("\t")[1] + ":" + "000@gmail.com" + ":user" + "\n"

#####################################################################################

write_string = '''# users.auth.php
# <?php exit()?>
# Don't modify the lines above
#
# Userfile
#
# Format:
#
# login:passwordhash:Real Name:email:groups,comma,seperated


''' + write_data

open("users.auth.php", "w", encoding="utf-8").write(write_string)
